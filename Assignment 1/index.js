const form = document.getElementById("form");

const email = document.getElementById("email");
const password = document.getElementById("password");
const sex = document.getElementById("sex");
const role = document.getElementById("roleGroup");
const perm = document.getElementById("permGroup");

const result = document.getElementById("result");

form.addEventListener("submit", (e) => {
  e.preventDefault();

  if (validateInputs()) {
    updateResponse();
  }
});

const validateInputs = () => {
  const emailValue = email.value.trim();
  const passwordValue = password.value.trim();
  const sexValue = sex.value;
  const roleValue = role.querySelector('input[name="userRole"]:checked');
  const permValues = perm.querySelectorAll('input[name="userPerm"]:checked');

  if (emailValue === "") {
    setError(email, "Email is required");
    return false;
  } else if (!isValidEmail(emailValue)) {
    setError(email, "Provide a valid email address");
    return false;
  } else {
    setSuccess(email);
  }

  if (passwordValue === "") {
    setError(password, "Password is required");
    return false;
  } else if (passwordValue.length < 6) {
    setError(password, "Password must be at least 6 character");
    return false;
  } else if (!/\d/.test(passwordValue)) {
    setError(password, "Password must contain 1 digit");
    return false;
  } else if (!/[A-Z]/.test(passwordValue)) {
    setError(password, "Password must contain 1 uppercase letter");
    return false;
  } else if (!/[a-z]/.test(passwordValue)) {
    setError(password, "Password must contain 1 lowercase letter");
    return false;
  } else {
    setSuccess(password);
  }

  if (sexValue === "") {
    setError(sex, "Sex is required");
    return false;
  } else {
    setSuccess(sex);
  }

  if (roleValue === null) {
    setError(role, "Role is required");
    return false;
  } else {
    setSuccess(role);
  }

  if (permValues.length === 0) {
    setError(perm, "Permissions is required");
    return false;
  } else if (permValues.length < 2) {
    setError(perm, "Add atleast 2 permissions");
    return false;
  } else {
    setSuccess(perm);
  }

  return true;
};

const updateResponse = () => {
  form.classList.add("success");
  response.classList.add("success");

  document.getElementById("emailResponse").innerText = email.value;

  document.getElementById("passwordResponse").innerText = password.value;

  document.getElementById("sexResponse").innerText = sex.value;

  document.getElementById("roleResponse").innerText = role.querySelector(
    'input[name="userRole"]:checked'
  ).value;

  let menu = document.getElementById("permResponse");

  let values = [];
  perm.querySelectorAll('input[name="userPerm"]:checked').forEach((radio) => {
    values.push(radio.value);
  });

  values.map((item, index) => {
    menu.innerHTML = menu.innerHTML + `${index + 1}. ${item} <br/>`;
  });

  console.log(values);
};

const setError = (element, message) => {
  const inputControl = element.parentElement;
  const errorDisplay = inputControl.querySelector(".error");
  errorDisplay.innerText = message;
};

const setSuccess = (element) => {
  const inputControl = element.parentElement;
  const errorDisplay = inputControl.querySelector(".error");
  errorDisplay.innerText = "";
};

const isValidEmail = (email) => {
  const re =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
};
