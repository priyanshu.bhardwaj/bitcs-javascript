let data = [
  {
    name: "priyanshu",
    age: 22,
    salary: 14000,
    sex: "male",
  },
  {
    name: "Dev",
    age: 20,
    salary: 18000,
    sex: "male",
  },
  {
    name: "siri",
    age: 25,
    salary: 30000,
    sex: "female",
  },
];

module.exports = data;
