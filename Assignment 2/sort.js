var data = require("./dummyData");

class Persons {
  constructor(arr, key, order) {
    this.array = arr;
    this.key = key;
    this.order = order;
  }

  quickSort() {
    let newArr = JSON.parse(JSON.stringify(this.array));
    for (var i = 0; i < newArr.length; i++) {
      var currVal = newArr[i][this.key];
      if (this.key === "name" || this.key === "sex") {
        currVal = currVal.toLowerCase();
      }
      var currElem = newArr[i];
      var j = i - 1;
      if (this.order === "asc") {
        while (j >= 0 && newArr[j][this.key] > currVal) {
          newArr[j + 1] = newArr[j];
          j--;
        }
      } else {
        while (j >= 0 && newArr[j][this.key] < currVal) {
          newArr[j + 1] = newArr[j];
          j--;
        }
      }
      newArr[j + 1] = currElem;
    }
    console.log("sorted array - ", newArr);
    console.log("original array - ", this.array);
  }
}

let obj = new Persons(data, "name", "asc");
obj.quickSort();
